!-------------------------------------------------------------------
!     Time-stamp: "2024-10-09 13:33:50 dwilliams"

      subroutine genANN_ctrans(pat_in)
      implicit none

      include 'nnparams.incl'
      include 'JTmod.incl'

      real*8 pat_in(maxnin)

      real*8 raw_in(maxnin),off_in(maxnin),ptrans_in(7)
      real*8 r0
      real*8 a,b,xs,ys,xb,yb

      integer k

      off_in(1:7)=pat_in(1:7)
      r0=offsets(1)

!     transform primitives
!     recover raw distances from offset coords
      do k=1,3
         raw_in(k)=off_in(k)+offsets(1)
      enddo

      do k=1,3
         ptrans_in(k)=off_in(k)
      enddo

!     rescale ONO angles
      ptrans_in(4)=deg2rad*off_in(4)
      ptrans_in(5)=deg2rad*off_in(5)
      ptrans_in(6)=deg2rad*off_in(6)
!     rescale umbrella
      ptrans_in(7)=off_in(7)*deg2rad

!     compute symmetry coordinates

!     A (breathing)
      a=(ptrans_in(1)+ptrans_in(2)+ptrans_in(3))/dsqrt(3.0d0)
!     ES
      call prim2emode(ptrans_in(1:3),xs,ys)
!     EB
      call prim2emode(ptrans_in(4:6),xb,yb)
!     B (umbrella)
      b=ptrans_in(7)

!     overwrite input with output

      pat_in(pat_index(1))=a   ! 1
      pat_in(pat_index(2))=xs
      pat_in(pat_index(3))=ys
      pat_in(pat_index(4))=xb
      pat_in(pat_index(5))=yb
      pat_in(pat_index(6))=b
!     totally symmetric monomials
      pat_in(pat_index(7))=xs**2 + ys**2 ! 2
      pat_in(pat_index(8))=xb**2 + yb**2 ! 3
      pat_in(pat_index(9))=b**2          ! 9
      pat_in(pat_index(10))=xs*xb+ys*yb  ! 4
!     S^3, B^3
      pat_in(pat_index(11))=xs*(xs**2-3*ys**2) ! 5
      pat_in(pat_index(12))=xb*(xb**2-3*yb**2) ! 6
!     S^2 B, S B^2
      pat_in(pat_index(13))=xb*(xs**2-ys**2) - 2*yb*xs*ys  ! 7
      pat_in(pat_index(14))=xs*(xb**2-yb**2) - 2*ys*xb*yb  ! 8

      do k=11,14
         pat_in(pat_index(k))=tanh(0.1d0*pat_in(pat_index(k)))*10.0d0
      enddo

      contains

      subroutine prim2emode(prim,ex,ey)
      implicit none
!     Takes a 2D-vector prim and returns the degenerate modes x and y
!     following our standard conventions.

      real*8 prim(3),ex,ey

      ex=(2.0d0*prim(1)-prim(2)-prim(3))/dsqrt(6.0d0)
      ey=(prim(2)-prim(3))/dsqrt(2.0d0)

      end


      end subroutine
